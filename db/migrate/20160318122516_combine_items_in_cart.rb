class CombineItemsInCart < ActiveRecord::Migration

  def up
    #create only one position for each product in chart
    Cart.all.each do |cart|
      #count how many positions has each product
      sums = cart.line_items.group(:product_id).sum(:quantity)

      sums.each do |product_id, quantity|
        if quantity > 1
          #delete separate lines
          cart.line_items.where(product_id: product_id).delete_all

          #switch to one line
          item = cart.line_items.build(product_id: product_id)
          item.quantity = quantity
          item.save!
        end
      end
    end
  end

  def down
    #create line for each product
    LineItem.where("quantity > 1").each do |line_item|
      #add individuals lines
      line_item.quantity.times do
        LineItem.create cart_id: line_item.cart_id, product_id:line_item.product_id, quantity: 1
      end

      #destroy previous line
      line_item.destroy
    end
  end
end
