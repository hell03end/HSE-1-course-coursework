# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Product.delete_all
Product.create!(title: 'pie',
                description:
                    %{
                    <p>Such tasty pie!</p>
                    },
                image_url: 'pie.jpg',
                price: 2.00)
Product.create!(title: 'cake',
                description:
                    %{
                    <p>my granny's cake.</p>
                    },
                image_url:   'cake.jpg',
                price: 3.27)

Product.create!(title: 'coffee',
                description:
                    %{
                    <p>perfect coffee!</p>
                    },
                image_url: 'coffee.jpg',
                price: 4.95)

Product.create!(title: 'lemon',
                description:
                    %{
                    <p>yellow and sour...</p>
                    },
                image_url: 'lemon.jpg',
                price: 1.95)

Product.create!(title: 'nachos',
                description:
                    %{
                    <p>Nachos are a Tex-Mex dish from northern Mexico.</p>
                    },
                image_url: 'http://cdn.firstwefeast.com/assets/2012/10/nachos.jpg',
                price: 6.45)
