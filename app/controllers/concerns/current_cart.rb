#This is common controller for current sessions.

module CurrentCart
  extend ActiveSupport::Concern

  private

    def set_cart
      @cart = Cart.find(session[:cart_id]) #try to find cart with such cart id
    rescue ActiveRecord::RecordNotFound #if where are no such cart
      @cart = Cart.create #create new cart
      session[:cart_id] = @cart.id #save this cart in session
    end
end