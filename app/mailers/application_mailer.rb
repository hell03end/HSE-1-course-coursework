class ApplicationMailer < ActionMailer::Base
  default from: "dapchelkin@edu.hse.ru"
  layout 'mailer'
end
