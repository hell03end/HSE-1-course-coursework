class OrderNotifer < ApplicationMailer
  default from: 'Alex Dd. <dapchelkin@edu.hse.ru>' #

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifer.received.subject
  #
  def received(order)
    @order = order

    mail to: order.email, subject: 'Confirm your order from Kursach' #
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifer.shipped.subject
  #
  def shipped(order)
    @order = order

    mail to: order.email, subject: 'Your order from Kursach was shipped!' #
  end
end
