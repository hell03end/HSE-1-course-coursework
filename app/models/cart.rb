class Cart < ActiveRecord::Base
  has_many :line_items, dependent: :destroy #there can be many line_items; if we destroy cart, all line items in it should be destroyed

  def add_product(product_id)
    current_item = line_items.find_by(product_id: product_id) #try to find product_id in current cart

    if current_item #if such product exist
      current_item.quantity += 1 #increase it quantity
    else
      current_item = line_items.build(product_id: product_id) #create new line_items position
    end

    current_item #return current_item
  end

  #total price for all products in cart
  def total_price
    line_items.to_a.sum { |item| item.total_price } #transfer lines to array, count and sum their price
  end
end
