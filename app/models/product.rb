class Product < ActiveRecord::Base
  has_many :line_items #there can be many line_items
  has_many :orders, through: :line_items #

  before_destroy :ensure_not_referenced_by_any_line_item #product can be destroyed only if it doesn't depend to some cart

  #check different parameters
  validates :title, :description, :image_url, presence: true #fields aren't empty
  validates :title, length: { minimum: 3 } #title isn't to short
  validates :description, length: { minimum: 10 } #description isn't to short
  validates :price, numericality: { greater_than_or_equal_to: 0.01 } #price is correct != 0 (.00 is max accuracy)
  validates :title, uniqueness: true #there are no same titles
  validates :image_url, allow_blank: true, format: {
      with: /\.(gif|jpg|jpeg|png)\Z/i,
      message: 'Incorrect URL: only .png/.gif/.jpg images allowed.'
  } #only .gif|.jpg|.jpeg|.png formats are supported

  def self.latest
    Product.order(:updated_at).last #only changed elements will be displayed (for cache)
  end


  private

  #ensure that there are no ids linked to some product
  def ensure_not_referenced_by_any_line_item
    if line_items.empty? #tere are no line_items
      true
    else
      errors.add(:base, 'there are products here')
      false
    end
  end
end
