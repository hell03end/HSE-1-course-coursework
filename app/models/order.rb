class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy #

  PAYMENT_TYPES = [ "Check", "Credit card", "Purchase order" ]

  validates :name, :address, :email, presence: true
  validates :email, format: {
      with: /\.(ru|org|com|en)\Z/i,
      message: 'Incorrect email, should be: example@somemail.com'
  }
  validates :pay_type, inclusion: PAYMENT_TYPES #only from this list

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil #delete line item when cart
      line_items << item
    end
  end
end
