class LineItem < ActiveRecord::Base

  #if database has external keys it's model should has methods 'belongs_to' to each of them
  belongs_to :order #connection with order
  belongs_to :product #connection with product
  belongs_to :cart #connection with cart

  #total price for line item
  def total_price
    product.price * quantity
  end

end
