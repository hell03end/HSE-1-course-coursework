require 'test_helper'

#particular testing (models)
class ProductTest < ActiveSupport::TestCase
  fixtures :products #file of testing stand

  test "products attributes must not be empty" do
    product = Product.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
    assert product.errors[:image_url].any?
    assert product.errors[:price].any?
  end

  test "product price must be positive" do
    product = Product.new(title: "candies", description: "zzz", image_url: "x.png")

    #failure here. <- WHY?
    product.price = -1
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
                 product.errors[:price]

    product.price = 0
    assert product.invalid?
    assert_equal ["must be greater than or equal to 0.01"],
                 product.errors[:price]
=begin
    #smth here wrong
    product.price = 1
    assert product.valid?
=end
  end

  def new_product(image_url)
    Product.new(title: "candies", description: "zzz1231231231", image_url: image_url, price: 1)
  end

  test "image url" do
    ok = %w{ x.gif x.jpg x.jpeg x.png X.Gif X.PNG http://a.b.c/x/y/z/x.jpg }
    bad = %w{ x.doc x.txt x.gif/more x.gif.more }

    ok.each do |name|
      assert new_product(name).valid? "#{name} shouldn't be invalid"
    end

    bad.each do |name|
      assert new_product(name).invalid? "#{name} shouldn't be valid"
    end
  end

  test "length of data" do
    product = Product.new(title: "12", description: "zzz1231231231", image_url: products(:ruby).image_url, price: 1)

    #failure here. <- WHY?
    assert product.invalid?
    assert_equal ["is too short (minimum is 3 characters)"],
                 product.errors[:title]

    product.title = "123"
    product.description = "zzz"

    assert product.invalid?
    assert_equal ["is too short (minimum is 10 characters)"],
                 product.errors[:description]
  end

  test "product is not valid with a unique title" do
    product = Product.new(title: products(:ruby).title, description: "zzz", image_url: "1.jpg", price: 1)

    assert product.invalid?

    #standart error type:
    #assert_equal [I18n.translate('activerecord.errors.messages.taken')],
    #manual error type:
    assert_equal ["has already been taken"],
                 product.errors[:title]
  end
end
