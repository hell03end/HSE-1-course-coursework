require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
=begin
  test "should get new" do
    get :new
    assert_response :success
  end
=end

  test "should login" do
    dos = users(:one)
    post :create, name: dos.name, password: 'secret'
    assert_redirected_to admin_url
    assert_equal dos.id, session[:user_id]
  end

  test "should fail login" do
    dos = users(:one)
    post :create, name: dos.name, password: 'wrong'
    assert_redirected_to login_url
  end

  test "should logout" do
    delete :destroy
    assert_redirected_to store_url
  end

end
