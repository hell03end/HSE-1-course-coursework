require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :products

  test "buying a product" do
    #
    LineItem.delete_all
    Order.delete_all
    ruby_book = products(:ruby)

    #
    get "/" #go to main page
    assert_response :success
    assert_template "index"

    #
    xml_http_request :post, '/line_items', product_id: ruby_book.id
    assert_response :success

    cart = Cart.find(session[:cart_id])
    assert_equal 1, cart.line_items.size
    assert_equal ruby_book, cart.line_items[0].product

    #
    get "/orders/new"
    assert_response :success
    assert_template "new"

    #
    post_via_redirect "/orders",
                      order: {
                          name:   "Dave",
                          address: "123 Hey Ho",
                          email: "linkid@aljaja.com",
                          pay_type: "Check"
                      }
    assert_response :success
    assert_template "index"
    cart = Cart.find(session[:cart_id])
    assert_equal 0, cart.line_items.size #there are no products in cart

    #
    orders = Order.all
    assert_equal 1, orders.size
    order = orders[0]

    assert_equal "Dave", order.name
    assert_equal "123 Hey Ho", order.address
    assert_equal "linkid@aljaja.com", order.email
    assert_equal "Check", order.pay_type

    assert_equal 0, cart.line_items.size #<-- Problem Here! Expected: 1, Actual: 0
    line_item = order.line_items[0]
    assert_equal ruby_book, line_item.product

    #
    mail = ActionMailer::Base.deliveries.last
    assert_equal ["linkid@aljaja.com"], mail.to
    assert_equal 'Alex Dd. <dapchelkin@edu.hse.ru>', mail[:from].value
    assert_equal 'Confirm your order from Kursach', mail.subject
  end
end
